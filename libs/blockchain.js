'use strict';
var CryptoJS = require("crypto-js");



class Block {
    constructor(index, previousHash, timestamp, data, hash) {
        this.index = index;
        this.previousHash = previousHash.toString();
        this.timestamp = timestamp;
        this.data = data;
        this.hash = hash.toString();
    }
}


var getGenesisBlock = () => {
	let index 		= 0;
	let prevHash 	= "0";
	let timestamp 	= 1465154705;
	let data 		= "GENESIS BLOCK"
	let hash 		= calculateHash(index, prevHash, timestamp, data);
    return new Block(index, prevHash, timestamp, data, hash);
};


var calculateHashForBlock = (block) => {
    return calculateHash(block.index, block.previousHash, block.timestamp, block.data);
};

var calculateHash = (index, previousHash, timestamp, data) => {
    return CryptoJS.SHA256(index + previousHash + timestamp + data).toString();
};

var addBlock = (newBlock) => {
    if (isValidNewBlock(newBlock, getLatestBlock())) {
        blockchain.push(newBlock);
    }
};

var addBlockForce = (newBlock) => {
    blockchain.push(newBlock);
};

var generateNextBlock = (blockData) => {
    var previousBlock = getLatestBlock();
    var nextIndex = previousBlock.index + 1;
    var nextTimestamp = new Date().getTime() / 1000;
    var nextHash = calculateHash(nextIndex, previousBlock.hash, nextTimestamp, blockData);
    return new Block(nextIndex, previousBlock.hash, nextTimestamp, blockData, nextHash);
};


var isValidNewBlock = (newBlock, previousBlock) => {
    if (previousBlock.index + 1 !== newBlock.index) {
        console.log('invalid index');
        return false;
    } else if (previousBlock.hash !== newBlock.previousHash) {
        console.log('invalid previoushash');
        return false;
    } else if (calculateHashForBlock(newBlock) !== newBlock.hash) {
        console.log(typeof (newBlock.hash) + ' ' + typeof calculateHashForBlock(newBlock));
        console.log('invalid hash: ' + calculateHashForBlock(newBlock) + ' ' + newBlock.hash);
        return false;
    }
    return true;
};

var isValidChain = (blockchainToValidate) => {
    if (JSON.stringify(blockchainToValidate[0]) !== JSON.stringify(getGenesisBlock())) {
        return false;
    }
    var tempBlocks = [blockchainToValidate[0]];
    for (var i = 1; i < blockchainToValidate.length; i++) {
        if (isValidNewBlock(blockchainToValidate[i], tempBlocks[i - 1])) {
            tempBlocks.push(blockchainToValidate[i]);
        } else {
            return false;
        }
    }
    return true;
};


var replaceChain = (newBlocks) => {
    if (isValidChain(newBlocks) && newBlocks.length > blockchain.length) {
        console.log('Received blockchain is valid. Replacing current blockchain with received blockchain');
        blockchain = newBlocks;
        return true;
    } else {
        console.log('Received blockchain invalid');
        return false;
    }
};


var getLatestBlock = () => blockchain[blockchain.length - 1];
var getChain = () => {return blockchain;}
var push = (block) => {
	if(isValidNewBlock(block, getLatestBlock())){
		blockchain.push(block);
		return true;		
	}
	else
		return false;
}



//initiate new blockchain
var blockchain = [getGenesisBlock()];


module.exports = 
    { 
        getChain
        ,getLatestBlock
        ,generateNextBlock
        ,addBlock
        ,replaceChain
        ,push
        ,isValidNewBlock
        ,force: { addBlock: addBlockForce }
        ,Block 
    }