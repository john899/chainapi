'use strict';
var express 	= require("express");
var bodyParser 	= require('body-parser');

var blockchain 	= require('../libs/blockchain');
var p2pServer 	= require('./p2pServer');

var app = express();

app.use(bodyParser.json());

//-- ROUTES
//BLOCKS
app.get('/blocks', (req, res) => res.json(blockchain.getChain()));

app.post('/blocks', (req, res) => {
    var newBlock = blockchain.generateNextBlock(req.body.data);
    blockchain.addBlock(newBlock);
    p2pServer.broadcastNewBlock();
    console.log('block added: ' + JSON.stringify(newBlock));
    res.json(newBlock);
});

app.post('/blocks/force', (req, res) => {
    const { index, previousHash, timestamp, data, hash } = req.body;
    let newBlock = new blockchain.Block(index, previousHash, timestamp, data, hash);
    blockchain.force.addBlock(newBlock);
    p2pServer.broadcastNewBlock();
    console.log('block added: ' + JSON.stringify(newBlock));
    res.json(newBlock);
});

//PEERS
app.get('/peers', (req, res) => {
    res.json(p2pServer.getSockets());
});

app.post('/peers', (req, res) => {
    p2pServer.connectToPeers([req.body.peer]);
    res.send(req.body.peer);
});


//starting http server
var runHttpServer = (http_port) => {
    app.listen(http_port, () => console.log('Listening http on port: ' + http_port));
};

module.exports = { runHttpServer } 