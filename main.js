'use strict';
var httpAPI 	= require('./services/httpAPI');
var p2pServer 	= require('./services/p2pServer');

var http_port = process.env.HTTP_PORT || 3001;
var p2p_port  = process.env.P2P_PORT || 6001;
var initialPeers = process.env.PEERS ? process.env.PEERS.split(',') : [];


httpAPI.runHttpServer(http_port);
p2pServer.runP2PServer(p2p_port, initialPeers);