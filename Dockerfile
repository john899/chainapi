FROM node:4.6

RUN mkdir /chainAPI
ADD package.json /chainAPI/
ADD main.js /chainAPI/

RUN cd /chainAPI && npm install

EXPOSE 3001
EXPOSE 6001

ENTRYPOINT cd /chainAPI && npm install && PEERS=$PEERS npm start